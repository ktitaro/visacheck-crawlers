const puppeteer = require('puppeteer')

async function main() {
  const browser = await puppeteer.launch({ headless: true })
  await require(process.env.PARSER)(browser)
  await browser.close()
}

(async () => {
  let status = 0
  try {
    await main()
  } catch(err) {
    console.error(err)
    status = 1
  } finally {
    process.exit(status)
  }
})()
