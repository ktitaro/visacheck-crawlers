const writeToFile = require('./utils/write_to_file')

module.exports = async (browser) => {
  console.group('Parsing currencies codes...')

  const url = 'https://www.iban.com/currency-codes'

  const page = await browser.newPage()
  await page.goto(url, { waitUntil: 'domcontentloaded' })

  const data = await page.evaluate(() => {
    return Array
      .from(document.querySelectorAll('.table tr'))
      .slice(1,)
      .map(item => {
        const country = item.children[0].textContent.trim()
        const currency = item.children[1].textContent.trim()
        const alpha_3 = item.children[2].textContent.trim()
        const numeric = item.children[3].textContent.trim()
        return {
          country,
          currency,
          alpha_3,
          numeric,
        }
      })
  })

  await page.close()
  await writeToFile('currencies.json', JSON.stringify(data))

  console.log('Done.')
  console.groupEnd()
}
