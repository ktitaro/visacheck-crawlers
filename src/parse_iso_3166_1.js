const writeToFile = require('./utils/write_to_file')
const parseEnVersion = require('./parse_iso_3166_1_en')
const parseRuVersion = require('./parse_iso_3166_1_ru')

module.exports = async (browser) => {
  console.group('Parsing ISO 3166-1...')

  let data
  data = await parseEnVersion(browser)
  // data = await parseRuVersion(browser, data)

  await writeToFile('iso_3166_1.json', JSON.stringify(data))

  console.log('Done.')
  console.groupEnd()
}
