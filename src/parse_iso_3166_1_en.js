module.exports = async (browser) => {
  console.group('Parsing english version...')

  const url = 'https://en.wikipedia.org/wiki/ISO_3166-1'

  const page = await browser.newPage()
  await page.goto(url, { waitUntil: 'domcontentloaded' })

  const data = await page.evaluate(() => {
    return Array
      .from(document.querySelectorAll('.wikitable:nth-of-type(2) tr'))
      .slice(1,)
      .map(item => {
        const name = item.children[0].textContent.trim()
        const href = item.children[0].querySelector('a').href
        const alpha_2 = item.children[1].textContent.trim()
        const alpha_3 = item.children[2].textContent.trim()
        const numeric = item.children[3].textContent.trim()
        const independent = item.children[5].textContent.trim() === 'Yes'
        return {
          name,
          href,
          alpha_2,
          alpha_3,
          numeric,
          independent,
        }
      })
  })

  await page.close()

  console.log('Done.')
  console.groupEnd()

  return data
}
