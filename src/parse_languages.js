const writeToFile = require('./utils/write_to_file')
const parseEnVersion = require('./parse_languages_en')
const parseRuVersion = require('./parse_languages_ru')

module.exports = async (browser) => {
  console.group('Parsing languages codes...')

  let data
  data = await parseEnVersion(browser)
  data = await parseRuVersion(browser, data)

  await writeToFile('languages.json', JSON.stringify(data))

  console.log('Done.')
  console.groupEnd()
}
