module.exports = async (browser) => {
  console.group('Parsing english version...')

  const url = 'https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes'

  const page = await browser.newPage()
  await page.goto(url, { waitUntil: 'domcontentloaded' })

  const data = await page.evaluate(() => {
    return Array
      .from(document.querySelectorAll('.wikitable#Table tr'))
      .slice(1,)
      .map(item => {
        const name = item.children[2].textContent.trim()
        const href = item.children[2].querySelector('a').href
        const iso_639_1 = item.children[4].textContent.trim()
        const iso_639_2_t = item.children[5].textContent.trim()
        const iso_639_2_b = item.children[6].textContent.trim()
        const iso_639_3 = item.children[7].textContent.trim().split(' ')[0]
        return {
          name,
          href,
          iso_639_1,
          iso_639_2_t,
          iso_639_2_b,
          iso_639_3,
        }
      })
  })

  await page.close()

  console.log('Done.')
  console.groupEnd()

  return data
}
