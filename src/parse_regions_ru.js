async function parseRegion(browser, url) {
  const page = await browser.newPage()
  await page.goto(url, { waitUntil: 'domcontentloaded' })

  const url_ru = await page.evaluate(() => {
    return Array
      .from(document.querySelectorAll('li.interlanguage-link.interwiki-ru > a'))
      .find(item => item.textContent.trim().match(/Русский/))
      .href
  })

  if (!url_ru) {
    throw new Error('Unable to parse russian version')
  }

  await page.goto(url_ru, { waitUntil: 'domcontentloaded' })

  const data = await page.evaluate(() => {
    const name = document
      .querySelector('h1#firstHeading')
      .textContent
      .trim()
    return { name, }
  })

  await page.close()

  return data
}

module.exports = async (browser, data) => {
  console.group('Parsing russian version...')

  const result = []

  for (const item of data) {
    const moreData = await parseRegion(browser, item.href)
    result.push({
      name_en: item.name,
      name_ru: moreData.name,
    })
  }

  console.log('Done.')
  console.groupEnd()

  return result
}
