module.exports = async (browser) => {
  console.group('Parsing english version...')

  const url = 'https://en.wikipedia.org/wiki/United_Nations_geoscheme'

  const page = await browser.newPage()
  await page.goto(url, { waitUntil: 'domcontentloaded' })

  const data = await page.evaluate(() => {
    let items
    items = Array.from(document.querySelectorAll('.mw-parser-output h2 ~ ul li > a'))
    items = items.slice(0, items.length - 4)
    items.splice(1,1)
    items.splice(5,1)

    return items.map(item => {
      const name = item.textContent.trim()
      const href = item.href
      return {
        name,
        href,
      }
    })
  })

  await page.close()

  console.log('Done.')
  console.groupEnd()

  return data
}
