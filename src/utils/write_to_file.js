const fs = require('fs').promises
const path = require('path')

const BASE_DIR = path.resolve(__dirname, '..', '..', 'output')

module.exports = async (filename, content) => {
  const filepath = path.join(BASE_DIR, filename)
  await fs.writeFile(filepath, content)
}
